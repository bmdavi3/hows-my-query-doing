#!/usr/bin/env bash

mkdir -p ~/hmqd
rm -rf ~/hmqd/*
cp ~/hows-my-query-doing/demo_directory/* ~/hmqd/
psql -h localhost -U postgres --file=reset.sql
