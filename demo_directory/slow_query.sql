-- DROP TABLE IF EXISTS small;
-- CREATE TABLE small (
--   some_val int
-- );


-- INSERT INTO small
-- SELECT
--   gs
-- FROM
--   generate_series(1, 100) AS gs;
--
--
--
-- DROP TABLE IF EXISTS big;
-- CREATE TABLE big (
--   other_val int
-- );
--
--
-- INSERT INTO big
-- SELECT
--   gs % 100 + 1
-- FROM
--   generate_series(1, 50000000) AS gs;
--
-- CREATE INDEX ON big (other_val);
--
-- VACUUM ANALYZE small;
-- VACUUM ANALYZE big;


-- SELECT
--   some_val,
--   (
--     SELECT
--       count(*)
--     FROM
--       big AS b
--     WHERE
--       b.other_val = s.some_val
--   )
-- FROM
--   small AS s;


-- DROP SEQUENCE IF EXISTS my_sequence;
-- CREATE SEQUENCE my_sequence;









-- SELECT
--   some_val,
--   count(*),
--   nextval('my_sequence')
-- FROM
--   small AS s INNER JOIN
--   big AS b ON
--     s.some_val = b.other_val
-- GROUP BY
--   some_val;



-- UPDATE
--   big
-- SET
--   other_val = other_val + 1;


-- Method 1
-- UPDATE
--   big
-- SET
--   other_val = other_val + 1 + nextval('my_sequence') * 0;


-- Method 2
-- UPDATE
--   big
-- SET
--   other_val = other_val + 1
-- RETURNING
--   nextval('my_sequence');



-- DELETE FROM
--   big
-- RETURNING
--   nextval('my_sequence');









-- ALTER TABLE big ADD COLUMN dummy_value int;
-- ALTER TABLE big ALTER COLUMN dummy_value SET DEFAULT nextval('my_sequence');
--
-- INSERT INTO big
-- SELECT
--   gs % 100 + 1  -- + nextval('my_sequence') * 0
-- FROM
--   generate_series(1, 25000000) AS gs
-- RETURNING
--   nextval('my_sequence')
-- ;











-- If needed
-- ALTER SEQUENCE my_sequence RESTART WITH 1;
